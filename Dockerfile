# This file is a template, and might need editing before it works on your project.
FROM python:2.7

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN pip install virtualenv

RUN virtualenv flask; source bin/activate

RUN pip install --no-cache-dir -r requirements.txt

COPY app.py /usr/src/app/

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World

CMD ["python", "app.py"]