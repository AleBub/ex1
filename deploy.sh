#!/bin/bash

apt-get update && apt-get install -y -qq wget
#wget https://download.docker.com/linux/debian/dists/buster/pool/stable/amd64/docker-ce-cli_18.09.6~3-0~debian-buster_amd64.deb
#dpkg -i docker-ce-cli_18.09.6~3-0~debian-buster_amd64.deb
#docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
wget -q https://mirror.openshift.com/pub/openshift-v4/clients/oc/4.1/linux/oc.tar.gz
apt-get install -y -qq tar && tar -xzf oc.tar.gz && rm -r oc.tar.gz
./oc login --token=$OC_TOKEN --server=$OC_SERVER
#./oc delete project ex1 --wait=true
./oc status
./oc new-project ex1
sleep 20
./oc new-app registry.gitlab.com/alebub/ex1 --name ex1-app
#./oc new-app -f deployment-template.yaml
./oc expose svc ex1-app --name ex1-welcome
# --name=welcome --port=4000